import os
import sys
from PIL import Image
import xml.etree.ElementTree as ET
from xml.etree import cElementTree as ElementTree

from PyQt5.Qt import QSvgRenderer
from PyQt5.QtGui import QImage, QPainter

SVG_DIR = "./svg"
PNG_DIR = "./png"

CONFIG_FILE = "./config.xml"


class XmlListConfig(list):
    def __init__(self, aList):
        for element in aList:
            if element:
                if len(element) == 1 or element[0].tag != element[1].tag:
                    self.append(XmlDictConfig(element))
                elif element[0].tag == element[1].tag:
                    self.append(XmlListConfig(element))
            elif element.text:
                text = element.text.strip()
                if text:
                    self.append(text)


class XmlDictConfig(dict):
    def __init__(self, parent_element):
        if parent_element.items():
            self.update(dict(parent_element.items()))
        for element in parent_element:
            if element:
                if len(element) == 1 or element[0].tag != element[1].tag:
                    aDict = XmlDictConfig(element)
                else:
                    aDict = {element[0].tag: XmlListConfig(element)}
                if element.items():
                    aDict.update(dict(element.items()))
                self.update({element.tag: aDict})
            elif element.items():
                self.update({element.tag: dict(element.items())})
            else:
                self.update({element.tag: element.text})


def make_square(im, size=(100, 100), bg=(255, 0, 0, 0)):
    im.thumbnail(size, Image.ANTIALIAS)
    x, y = im.size
    new_im = Image.new('RGBA', size, bg)
    new_im.paste(im, (int((size[0] - x) / 2), int((size[1] - y) / 2)))
    return new_im


def svgtopng(svg_file, png_file, size=None, bg=(255, 0, 0, 0)):
    width = size[0]
    r = QSvgRenderer(svg_file)
    height = int(r.defaultSize().height()*width/r.defaultSize().width())
    i = QImage(width, height, QImage.Format_ARGB32)
    p = QPainter(i)
    r.render(p)
    i.save(png_file)
    p.end()
    im = Image.open(png_file)
    if im.mode == "RGBA":
        if bg[3] != 0:
            datas = im.getdata()
            newData = []
            for item in datas:
                if item[3] == 0:
                    newData.append(bg)
                else:
                    newData.append(item)
            im.putdata(newData)
        if size:
            im = make_square(im, size, bg)
        im.save(png_file, "PNG")


def load_config(path=CONFIG_FILE):
    if not os.path.isfile(path):
        print("Config File doesn't exist!")
        sys.exit(1)
    tree = ElementTree.parse(path)
    root = tree.getroot()
    xmldict = XmlDictConfig(root)
    width = int(xmldict["width"])
    height = int(xmldict["height"])
    bg = xmldict["backgroundcolor"]
    R = int(bg[:2], 16)
    G = int(bg[2:4], 16)
    B = int(bg[4:], 16)
    return (width, height), (R, G, B, 255)


def get_files(path=SVG_DIR):
    if not os.path.isdir(path):
        os.mkdir(path)
        print(f"Directory not Exist!\n {path} created!")
        sys.exit(1)
    files = []
    for r, _, f in os.walk(path):
        for file in f:
            if '.svg' in file:
                files.append(os.path.join(r, file))
    return files


def main():
    if not os.path.isdir(PNG_DIR):
        os.mkdir(PNG_DIR)
    size, bg = load_config()
    files = get_files()
    for f in files:
        name = os.path.basename(f)
        print(f"Converting {name}...")
        name = name.split('.')[0]
        name += f"_{size[0]}_{size[1]}"
        name += f"_{'%02x' % bg[0]}"
        name += f"{'%02x' % bg[1]}"
        name += f"{'%02x' % bg[2]}"
        name += ".png"
        png_path = os.path.join(
            PNG_DIR, name)
        svgtopng(f, png_path, size, bg)


if __name__ == '__main__':
    main()
